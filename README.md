Bài 1: Nhập thông tin cá nhân( tên, tuổi, địa chỉ,...) và in ra thông tin đó

Bài 2: Hiển thị từ cách nhau bởi ký tự "--" ra màn hình. vd: Toi--ten--la--Nam

Bài 3: Tạo một chương trình yêu cầu người dùng nhập tên và tuổi của họ. In ra màn hình cho biết năm họ sẽ tròn 100 tuổi.

Bài 4: Viết chương trình làm tròn số thập phân A đến B chữ số sau dấu phẩy. A và B được nhập bất kỳ từ bàn phím. Hiển thị số A sau khi được làm tròn ra màn hình.
VD: input: Nhập 22.1098 và 3
    output: 22.110

Bài 5: Nhập vào từ bàn phím 1 ký tự bất kỳ, kiểm tra xem ký tự vừa nhập vào thuộc kiểu dữ liệu nào?

Bài 6: Nhập vào từ màn hình 2 số và tính giá trị: tổng, hiệu, tích, thương, lũy thừa của 2 số đó

Bài 7: Nhập vào từ bàn phím các cạnh và tính chu vi, diện tích hình chữ nhật

Bài 8: Nhập từ bán phím vào bán kính và tính chu vi,diện tích hình tròn

Bài 9: Tính diện tích xung quanh của 1 hình trụ có chu vi đáy là 13 và chiều cao là 3

Bài 10: Tính giá trị biểu thức sau: ((x+y+z)^2)^3 + (x^4+2y^3+2z)^2 với x, y, z nhập từ bàn phím
